// path: ./ecosystem.config.js

module.exports = {
  apps: [
    {
      name: 'mapetiterobenwax',
      script: 'npm',
      args: 'start',
    },
  ],
};