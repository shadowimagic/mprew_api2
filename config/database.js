/*const path = require('path');

module.exports = ({ env }) => ({
  connection: {
    client: 'sqlite',
    connection: {
      filename: path.join(__dirname, '..', env('DATABASE_FILENAME', '.tmp/data.db')),
    },
    useNullAsDefault: true,
  },
});*/

module.exports = ({ env }) => ({
  connection: {
    client: 'mysql',
    connection: {
      host: env('DATABASE_HOST', 'localhost'),
      port: env.int('DATABASE_PORT', 3306),
      database: env('DATABASE_NAME', 'mprewdb'),
      user: env('DATABASE_USERNAME', 'mprew'),
      password: env('DATABASE_PASSWORD', 'lion@3107'),
      ssl: env.bool('DATABASE_SSL', false),
    },
  },
});

