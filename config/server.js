// module.exports = ({ env }) => ({
//   host: env('HOST', '51.38.165.24'),
//   port: env.int('PORT', 1338),
//   url: "https://backend.mia-dreams.com",
//   app: {
//     keys: env.array('APP_KEYS'),
//   }
// });
module.exports = ({ env }) => ({
  host: env('HOST', 'localhost'),
  port: env.int('PORT', 1337),
//  url: "https://backend.mia-dreams.com",
  app: {
    keys: env.array('APP_KEYS'),
  }
});
